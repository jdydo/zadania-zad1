
Laboratorium #1
===============

1. Zbudować serwer iteracyjny, który po przyłączeniu klienta zwraca mu kolejny numer i zamyka połączenie.
   Przetestować działanie używając komendy telnet.

2. Zbudować serwer echo i klienta do niego.

Dla każdego zadania proszę się upewnić, że przechodzą testy.