# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 32111)  # TODO: zmienić port!
    # Ustawienie licznika na zero
    count = 0
    # Tworzenie gniazda TCP/IP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
    # Powiązanie gniazda z adresem
    s.bind(server_address)
    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    s.listen(1)
    try:
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')
            connection, address = s.accept()
            # Nawiązanie połączenia
            addr = address
            logger.info(u'połączono z {0}:{1}'.format(*addr))
            # Podbicie licznika
            count += 1
            try:
                # Wysłanie wartości licznika do klienta
                connection.sendall(str(count))
                logger.info(u'wysłano {0}'.format(count))
            finally:
                # Zamknięcie połączenia
                connection.close()
                logger.info(u'zamknięto połączenie')
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('iteration_server')
    server(logger)
    sys.exit(0)
