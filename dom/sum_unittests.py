# -*- coding: utf-8 -*-

import unittest
from sum_client import sum_function


class SumTestCase(unittest.TestCase):

    def test_sum(self):
        num1 = 128
        num2 = 512

        actual = sum_function(num1, num2)
        expected = str(640)

        self.assertEqual(expected, actual)

    def test_sum_error1(self):
        num1 = 'abc'
        num2 = 512

        actual = sum_function(num1, num2)
        expected = u'BŁĄD! Wprowadzono złe dane!'

        self.assertEqual(expected, actual)

    def test_sum_error2(self):
        num1 = 512
        num2 = 'abc'

        actual = sum_function(num1, num2)
        expected = u'BŁĄD! Wprowadzono złe dane!'

        self.assertEqual(expected, actual)

    def test_sum_error3(self):
        num1 = 'abc'
        num2 = 'abc'

        actual = sum_function(num1, num2)
        expected = u'BŁĄD! Wprowadzono złe dane!'

        self.assertEqual(expected, actual)

    def test_sum_error4(self):
        num1 = None
        num2 = None

        actual = sum_function(num1, num2)
        expected = u'BŁĄD! Wprowadzono złe dane!'

        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
