
Praca domowa #1
===============

Stworzyć serwer przyjmujący dwie liczby i zwracający ich sumę oraz przykładowego interaktywnego klienta.
Serwer powinien zostać uruchomiony na maszynie laboratoryjnej i działać po wylogowaniu z niej. W tym zadaniu
pomoże komenda ``screen`` (dokumentacja po wpisaniu komendy ``man screen``).

Dla wykazania poprawności stworzonego programu proszę napisać testy jednostkowe.