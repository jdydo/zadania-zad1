# -*- coding: utf-8 -*-

import socket


def sum_function(num1, num2):

    if not num1 or not num2:
        return u'BŁĄD! Wprowadzono złe dane!'

    host = '194.29.175.240'
    port = 32115

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

    try:
        s.connect((host, port))

        s.sendall(str(num1))
        s.sendall(str(num2))
        data = s.recv(1024).decode('utf-8')
    except socket.error:
        data = u'Błąd! Brak połączenia z serwerem!'
    finally:
        s.close()

    return data


if __name__ == '__main__':
    a = raw_input(u'Wprowadź liczbę całkowitą A: ')
    b = raw_input(u'Wprowadź liczbę całkowitą B: ')
    print 'Wynik: ' + sum_function(a, b)