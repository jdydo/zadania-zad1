# -*- coding: utf-8 -*-

import socket


host = '194.29.175.240'
port = 32115

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host, port))
s.listen(1)

print 'Serwer ONLINE'

try:
    while True:
        client, address = s.accept()
        try:
            a = client.recv(1024)
            b = client.recv(1024)
            ab = int(a) + int(b)
            client.sendall(str(ab))
            print str(address) + ' -> ' + str(b) + '+' + str(a) + '=' + str(ab)
        except ValueError:
            client.send('BŁĄD! Wprowadzono złe dane!')
            print str(address) + ' -> ' + u'BŁĄD! Wprowadzono złe dane!'
        except socket.error:
            print str(address) + ' -> ' + u'BŁĄD! Połączenie przerwane!'
        finally:
            client.close()
except KeyboardInterrupt:
    s.close()